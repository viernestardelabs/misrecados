# Welcome to MisRecados

MisRecados multi-platform app. Technologies.

* `GWT`
* `MGWT`
* `GwtPhonegap`
* `Cordova`
* `PhoneGap`*

*PhoneGap App and PhoneGap Build tools.

### Configure the project
```
> git clone git@bitbucket.org:viernestardelabs/misrecados.git
> git clone git@bitbucket.org:viernestardelabs/misrecados-app.git
```

Make sure than `misrecados/www` link points to `${webappDirectory}` folder of
[misrecados-app](https://bitbucket.org/viernestardelabs/misrecados-app) maven
project.

### Run and Debug

**Generate `www` content**

This project uses GWT, so you need to generate the HTML/JS/CSS code before you
can run the project. There are two alternatives.

1. Compile project

    `> cd misrecados-app && mvn package`

2. Dev mode

    `> cd misrecados-app && mvn gwt:run`

    After this command, you can test your application on chrome at http://localhost:8888.
    To debug use Chrome Dev Tools (GWT uses source maps for debugging). To
    reload changes just refresh the page.

**Native run**

Requires `www`content. Alternatives:

1. Using [phonegap-app](http://app.phonegap.com/)

    `> cd misrecados && phonegap serve`

2. Native development using cordova

    `> cd misrecados && cordova platform add <platform> && cordova run <platform>`

    The `platform add` command is only required one time for each platform.

3. Native development using phonegap

    `> cd misrecados && phonegap run <platform>`

    Tested using `android` and `ios` platforms.
